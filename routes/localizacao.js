const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const multer = require('multer');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const login = require('../middleware/login');


const upload = multer({ dest: 'uploads/' });

// retorna
router.get('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM localizacao;',
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});
// inseri
router.post('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'INSERT INTO localizacao (uf, logradouro, cep, numero, idUser, idUserFunc ) VALUES (?,?,?,?,?,?)',
            [req.body.uf, req.body.logradouro, req.body.cep, req.body.numero, req.body.idUser, req.body.idUserFunc],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'localizacao cadastrado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});

router.get('/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM localizacao WHERE idUser = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

router.get('/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM localizacao WHERE id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

// altera
router.patch('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
            conn.query(
                'UPDATE localizacao SET uf = ?, logradouro = ?, cep = ?, numero = ?, idUser = ?, idUserFunc = ? WHERE id = ?',
                [req.body.uf, req.body.logradouro, req.body.cep, req.body.numero, req.body.idUser, req.body.idUserFunc],
                (error, resultado, field) => {
                    conn.release();
                    if (error) { return res.status(500).send({ error: error }) }
                    res.status(201).send({
                        mensagem: 'localizacao alterado com sucesso',
                        id: resultado.insertId
                    });
                }
            )
    });
});




module.exports = router;