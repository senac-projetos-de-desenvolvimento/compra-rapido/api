const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const multer = require('multer');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const login = require('../middleware/login');


// retorna
router.get('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM contato;',
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});
// inseri
router.post('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'INSERT INTO contato (telefone, email, User_idUser, userFunc_iduserFunc) VALUES (?,?,?,?)',
            [req.body.telefone, req.body.email, req.body.User_idUser, req.body.userFunc_iduserFunc],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'contato cadastrado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});



module.exports = router;