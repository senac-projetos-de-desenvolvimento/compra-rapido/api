const express = require('express');
const router = express.Router();
const mysql = require('../mysql').pool;
const login = require('../middleware/login');


// retorna
router.get('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM listaCompras;',
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

// retorna
router.get('/trat/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT l.id, l.listaProd, (u.nome) as cliente,(c.nome) as entregador, l.dataHoraPed, l.dataHoraEnt, l.situacao, (lc.logradouro) AS endereco, (lc.id) AS idlocal FROM listaCompras AS l INNER JOIN user AS u ON (l.idUser = u.id) LEFT JOIN userFunc AS c ON (l.idUserFunc = c.id) INNER JOIN localizacao AS lc ON (l.idlocalizacao = lc.id);',
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});
// inseri
router.post('/', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        
        var dt = new Date();
        let datahora = dt.getYear() + "-" + dt.getMonth() + "-" + dt.getDate() + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
        
        conn.query(
            'INSERT INTO listaCompras (listaProd, dataHoraPed, dataHoraEnt, situacao, idUser, idUserFunc, idlocalizacao) VALUES (?,?,?,?,?,?,?)',
            [req.body.listaProd, datahora, req.body.dataHoraEnt, req.body.situacao, req.body.idUser, req.body.idUserFunc, req.body.idlocalizacao],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'ListaCompras criado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });
});

router.get('/id/:id', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT * FROM Matchs WHERE id = ?;',
            [req.params.id],
            (error, resultado, fields) => {
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send({ response: resultado })
            }
        )
    });
});


router.get('/lista/:id', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT l.id, l.listaProd, (u.nome) as cliente,(c.nome) as entregador, l.dataHoraPed, l.dataHoraEnt, l.situacao, (lc.logradouro) AS endereco, (lc.id) AS idlocal FROM listaCompras AS l INNER JOIN user AS u ON (l.idUser = u.id) LEFT JOIN userFunc AS c ON (l.idUserFunc = c.id) INNER JOIN localizacao AS lc ON (l.idlocalizacao = lc.id) WHERE l.id = ?;',
            [req.params.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

router.get('/listaUser/:id', login, (req, res, next) => {

    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'SELECT l.id, l.listaProd, (u.nome) as cliente,(c.nome) as entregador, l.dataHoraPed, l.dataHoraEnt, l.situacao, (lc.logradouro) AS endereco, (lc.id) AS idlocal FROM listaCompras AS l INNER JOIN user AS u ON (l.idUser = u.id) LEFT JOIN userFunc AS c ON (l.idUserFunc = c.id) INNER JOIN localizacao AS lc ON (l.idlocalizacao = lc.id) WHERE u.id = ?;',
            [req.params.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                return res.status(200).send(resultado)
            }
        )
    });
});

// altera
router.patch('/', login, (req, res, next) => {
    mysql.getConnection((error, conn) => {
        if (error) { return res.status(500).send({ error: error }) }
        conn.query(
            'UPDATE Matchs SET gyms_id = ?, courts_id = ?, start_time = ?, end_time = ?, name = ?, type = ?, description = ?, responsible_id = ? WHERE id = ?',
            [req.body.gyms_id, req.body.courts_id, req.body.start_time, req.body.end_time, req.body.name, req.body.type, req.body.description, req.body.responsible_id, req.body.id],
            (error, resultado, field) => {
                conn.release();
                if (error) { return res.status(500).send({ error: error }) }
                res.status(201).send({
                    mensagem: 'matchs alterado com sucesso',
                    id: resultado.insertId
                });
            }
        )
    });

    router.delete('/', login, (req, res, next) => {
        mysql.getConnection((error, conn) => {
            if (error) { return res.status(500).send({ error: error }) }
            conn.query(
                'DELETE FROM Matchs WHERE id = ?;',
                [req.body.id],
                (error, resultado, field) => {
                    conn.release();
                    if (error) { return res.status(500).send({ error: error }) }
                    res.status(202).send({
                        mensagem: 'matchs Excluido com Sucesso'
                    });
                }
            )
        });
    });

});



module.exports = router;